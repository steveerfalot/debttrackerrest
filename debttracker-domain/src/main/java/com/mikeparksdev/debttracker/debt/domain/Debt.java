package com.mikeparksdev.debttracker.debt.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtor.domain.Debtor;

import javax.persistence.*;

@Entity
@Table(name = "debt")
public class Debt {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "debtee_id", referencedColumnName = "id")
    @JsonBackReference
    private Debtee debtee;

    @ManyToOne
    @JoinColumn(name = "debtor_id", referencedColumnName = "id")
    @JsonBackReference
    private Debtor debtor;

    @Column(name = "initial_amount")
    private Long initialAmount;

    @Column(name = "current_amount")
    private Long currentAmount;

    @Column(name = "remaining_amount")
    private Long remainingAmount;

    @Column(name = "interest_rate")
    private Long interestRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Debtee getDebtee() {
        return debtee;
    }

    public void setDebtee(Debtee debtee) {
        this.debtee = debtee;
    }

    public Long getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Long initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Long getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Long currentAmount) {
        this.currentAmount = currentAmount;
    }

    public Long getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Long remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public Long getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Long interestRate) {
        this.interestRate = interestRate;
    }

    public Debtor getDebtor() {
        return debtor;
    }

    public void setDebtor(Debtor debtor) {
        this.debtor = debtor;
    }
}
