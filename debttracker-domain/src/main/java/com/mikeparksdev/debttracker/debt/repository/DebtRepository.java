package com.mikeparksdev.debttracker.debt.repository;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebtRepository extends JpaRepository<Debt, Long> {
    List<Debt> findByDebteeId(Long id);
}
