package com.mikeparksdev.debttracker.debt.service;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DebtService {

    Debt findById(Long id);

    List<Debt> findAll();

    Debt findOne(Long id);

    List<Debt> findByDebteeId(Long id);

    Debt save(Debt debt);

    void delete(Long id);

}
