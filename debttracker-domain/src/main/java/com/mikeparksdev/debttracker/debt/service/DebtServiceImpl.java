package com.mikeparksdev.debttracker.debt.service;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.repository.DebtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DebtServiceImpl implements DebtService {

    @Autowired
    private DebtRepository debtRepository;

    @Override
    public Debt findById(Long id) {
        return debtRepository.findOne(id);
    }

    @Override
    public List<Debt> findAll() {
        return debtRepository.findAll();
    }

    @Override
    public Debt findOne(Long id) {
        return debtRepository.findOne(id);
    }

    @Override
    public List<Debt> findByDebteeId(Long id) {
        return debtRepository.findByDebteeId(id);
    }

    @Override
    public Debt save(Debt debt) {
        return debtRepository.save(debt);
    }

    @Override
    public void delete(Long id) {
        debtRepository.delete(id);
    }


}
