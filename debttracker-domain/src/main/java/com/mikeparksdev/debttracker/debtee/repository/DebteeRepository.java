package com.mikeparksdev.debttracker.debtee.repository;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DebteeRepository extends JpaRepository<Debtee, Long> {
}
