package com.mikeparksdev.debttracker.debtee.service;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DebteeService {

    Debtee findById(Long id);

    List<Debtee> findAll();

    Debtee save(Debtee debtee);

    void delete(Long id);
}
