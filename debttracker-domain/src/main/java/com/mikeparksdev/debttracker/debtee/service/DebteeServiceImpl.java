package com.mikeparksdev.debttracker.debtee.service;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtee.repository.DebteeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DebteeServiceImpl implements DebteeService {

    @Autowired
    private DebteeRepository debteeRepository;

    @Override
    public Debtee findById(Long id) {
        return debteeRepository.findOne(id);
    }

    @Override
    public List<Debtee> findAll() {
        return debteeRepository.findAll();
    }

    @Override
    public Debtee save(Debtee debtee) {
        return debteeRepository.save(debtee);
    }

    @Override
    public void delete(Long id){
        debteeRepository.delete(id);
    }

}
