package com.mikeparksdev.debttracker.debtor.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mikeparksdev.debttracker.debt.domain.Debt;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="debtor")
public class Debtor {

    public static final String DEBTOR = "debtor";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "debt")
    @OneToMany(mappedBy = DEBTOR)
    @JsonManagedReference
    private List<Debt> debts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Debt> getDebts() {
        return debts;
    }

    public void setDebts(List<Debt> debts) {
        this.debts = debts;
    }
}
