package com.mikeparksdev.debttracker.debtor.repository;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DebtorRepository extends JpaRepository<Debtor, Long> {
}
