package com.mikeparksdev.debttracker.debtor.service;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DebtorService {

    List<Debtor> findAll();

    Debtor findOne(Long id);

    Debtor save(Debtor debtor);

    void delete(Long id);

}
