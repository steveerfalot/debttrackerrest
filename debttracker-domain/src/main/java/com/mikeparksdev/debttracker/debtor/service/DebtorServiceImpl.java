package com.mikeparksdev.debttracker.debtor.service;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import com.mikeparksdev.debttracker.debtor.repository.DebtorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DebtorServiceImpl implements DebtorService {

    @Autowired
    private DebtorRepository debtorRepository;

    @Override
    public List<Debtor> findAll() {
        return debtorRepository.findAll();
    }

    @Override
    public Debtor findOne(Long id) {
        return debtorRepository.findOne(id);
    }

    @Override
    public Debtor save(Debtor debtor) {
        return debtorRepository.save(debtor);
    }

    @Override
    public void delete(Long id) {
        debtorRepository.delete(id);
    }
}
