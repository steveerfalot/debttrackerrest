package com.mikeparksdev.debttracker.debt.domain;

import com.mikeparksdev.AbstractDomainTest;
import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import org.junit.Test;

import static org.junit.Assert.*;

public class DebtTest extends AbstractDomainTest<Debt> {

    @Test
    @Override
    public void testSetGetVariables() {
        //arrange
        Debt testObject = new Debt();

        //act
        //assert
        baseTestSetGetVariables(testObject, Long.class, 1L, "id");
        baseTestSetGetVariables(testObject, String.class, "turd goblin", "name");
        baseTestSetGetVariables(testObject, Debtee.class, new Debtee(), "debtee");
        baseTestSetGetVariables(testObject, Debtor.class, new Debtor(), "debtor");
        baseTestSetGetVariables(testObject, Long.class, 2L, "initialAmount");
        baseTestSetGetVariables(testObject, Long.class, 2L, "currentAmount");
        baseTestSetGetVariables(testObject, Long.class, 2L, "remainingAmount");
        baseTestSetGetVariables(testObject, Long.class, 2L, "interestRate");
    }
}