package com.mikeparksdev.debttracker.debt.service;


import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.repository.DebtRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebtServiceImplTest {

    @Mock
    private DebtRepository mockDebtRepository;

    @InjectMocks
    private DebtServiceImpl subject;

    private Debt debt;
    private List<Debt> debts;

    @Before
    public void setUp() throws Exception {
        subject = new DebtServiceImpl();

        debt = new Debt();
        debts = new ArrayList<Debt>();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindById_callsFindOne(){
        when(mockDebtRepository.findOne(1L)).thenReturn(debt);

        subject.findById(1L);

        verify(mockDebtRepository).findOne(1L);
    }

    @Test
    public void testFindById_returnsDebt(){
        when(mockDebtRepository.findOne(1L)).thenReturn(debt);

        Debt actual = subject.findById(1L);

        assertEquals(debt, actual);
    }

    @Test
    public void testFindAll_callsFindAll(){
        when(mockDebtRepository.findAll()).thenReturn(debts);

        subject.findAll();

        verify(mockDebtRepository).findAll();
    }

    @Test
    public void testFindAll_returnsDebtList(){
        when(mockDebtRepository.findAll()).thenReturn(debts);

        List<Debt> actual = subject.findAll();

        assertEquals(debts, actual);
    }

    @Test
    public void testFindOne_callsFindOne(){
        when(mockDebtRepository.findOne(1L)).thenReturn(debt);

        subject.findOne(1L);

        verify(mockDebtRepository).findOne(1L);
    }

    @Test
    public void testFindOne_returnsDebt(){
        when(mockDebtRepository.findOne(1L)).thenReturn(debt);

        Debt actual = subject.findOne(1L);

        assertEquals(debt, actual);
    }

    @Test
    public void testFindByDebteeId_callsFindByDebteeId(){
        when(mockDebtRepository.findByDebteeId(1L)).thenReturn(debts);

        subject.findByDebteeId(1L);

        verify(mockDebtRepository).findByDebteeId(1L);
    }

    @Test
    public void testFindByDebteeId_returnsDebtList(){
        when(mockDebtRepository.findByDebteeId(1L)).thenReturn(debts);

        List<Debt> actual = subject.findByDebteeId(1L);

        assertEquals(debts, actual);
    }

    @Test
    public void testSave_callsSave(){
        when(mockDebtRepository.save(debt)).thenReturn(debt);

        subject.save(debt);

        verify(mockDebtRepository).save(debt);
    }

    @Test
    public void testSave_returnsSavedDebt(){
        when(mockDebtRepository.save(debt)).thenReturn(debt);

        Debt actual = subject.save(debt);

        assertEquals(debt, actual);
    }

    @Test
    public void testDelete_callsDelete(){
        subject.delete(1L);

        verify(mockDebtRepository).delete(1L);
    }

}