package com.mikeparksdev.debttracker.debtee.domain;

import com.mikeparksdev.AbstractDomainTest;
import com.mikeparksdev.debttracker.debt.domain.Debt;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DebteeTest extends AbstractDomainTest<Debtee>{

    @Test
    @Override
    public void testSetGetVariables() {
        //arrange
        Debtee testObject = new Debtee();

        //act
        //assert
        baseTestSetGetVariables(testObject, Long.class, 1L, "id");
        baseTestSetGetVariables(testObject, String.class, "Steve Dogface", "name");
        baseTestSetGetVariables(testObject, List.class, new ArrayList<Debt>(), "debts");
    }

}