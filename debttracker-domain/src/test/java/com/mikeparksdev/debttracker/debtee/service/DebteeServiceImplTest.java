package com.mikeparksdev.debttracker.debtee.service;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtee.repository.DebteeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebteeServiceImplTest {

    @Mock
    private DebteeRepository mockDebteeRepository;

    @InjectMocks
    private DebteeServiceImpl subject;

    @Before
    public void setUp() throws Exception {
        subject = new DebteeServiceImpl();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindById_callsFindOne(){
        //arrange
        when(mockDebteeRepository.findOne(1L)).thenReturn(new Debtee());
        //act
        subject.findById(1L);
        //assert
        verify(mockDebteeRepository).findOne(1L);
    }

    @Test
    public void testFindAll_callsFindAll(){
        //arrange
        when(mockDebteeRepository.findAll()).thenReturn(new ArrayList<Debtee>());
        //act
        subject.findAll();
        //assert
        verify(mockDebteeRepository).findAll();
    }

    @Test
    public void testSave_callsSave(){
        //arrange
        Debtee person = new Debtee();
        when(mockDebteeRepository.save(person)).thenReturn(person);
        //act
        subject.save(person);
        //assert
        verify(mockDebteeRepository).save(person);
    }

    @Test
    public void testDelete_callsDelete(){
        subject.delete(1L);

        verify(mockDebteeRepository).delete(1L);
    }
}