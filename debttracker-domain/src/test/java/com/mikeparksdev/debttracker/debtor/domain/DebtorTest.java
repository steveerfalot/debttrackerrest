package com.mikeparksdev.debttracker.debtor.domain;

import com.mikeparksdev.AbstractDomainTest;
import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by steveerf on 4/3/15.
 */
public class DebtorTest extends AbstractDomainTest<Debtor> {

    @Test
    @Override
    public void testSetGetVariables() {
        //arrange
        Debtor testObject = new Debtor();

        //act
        //assert
        baseTestSetGetVariables(testObject, Long.class, 1L, "id");
        baseTestSetGetVariables(testObject, String.class, "Steve Dogface", "name");
        baseTestSetGetVariables(testObject, List.class, new ArrayList<Debt>(), "debts");
    }

}