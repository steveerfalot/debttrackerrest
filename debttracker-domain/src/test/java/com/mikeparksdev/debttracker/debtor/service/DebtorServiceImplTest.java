package com.mikeparksdev.debttracker.debtor.service;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import com.mikeparksdev.debttracker.debtor.repository.DebtorRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebtorServiceImplTest {

    @Mock
    private DebtorRepository mockDebtorRepository;

    @InjectMocks
    private DebtorServiceImpl subject;

    private List<Debtor> debtors;
    private Debtor debtor;
    private static final long id = 1L;

    @Before
    public void setUp() throws Exception {

        subject = new DebtorServiceImpl();

        debtors = new ArrayList<Debtor>();

        debtor = new Debtor();
        debtor.setId(id);
        debtor.setName("Bob");

        debtors.add(debtor);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_callsFindAll() {
        when(mockDebtorRepository.findAll()).thenReturn(debtors);
        subject.findAll();
        verify(mockDebtorRepository).findAll();
    }

    @Test
    public void testFindAll_returnsDebtors() {
        when(mockDebtorRepository.findAll()).thenReturn(debtors);
        List<Debtor> actual = subject.findAll();
        assertEquals(debtors, actual);
    }

    @Test
    public void testFindOne_callsFindOne() {
        when(mockDebtorRepository.findOne(id)).thenReturn(debtor);
        subject.findOne(id);
        verify(mockDebtorRepository).findOne(id);
    }

    @Test
    public void testFindOne_returnsDebtor() {
        when(mockDebtorRepository.findOne(id)).thenReturn(debtor);
        Debtor actual = subject.findOne(id);
        assertEquals(debtor, actual);
    }

    @Test
    public void testSave_callsSave() {
        when(mockDebtorRepository.save(debtor)).thenReturn(debtor);
        subject.save(debtor);
        verify(mockDebtorRepository).save(debtor);
    }

    @Test
    public void testSave_returnsDebtor() {
        when(mockDebtorRepository.save(debtor)).thenReturn(debtor);
        Debtor actual = subject.save(debtor);
        assertEquals(debtor, actual);
    }

    @Test
    public void testDelete_callsDelete() {
        subject.delete(id);
        verify(mockDebtorRepository).delete(id);
    }

}