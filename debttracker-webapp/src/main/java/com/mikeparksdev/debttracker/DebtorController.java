package com.mikeparksdev.debttracker;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import com.mikeparksdev.debttracker.debtor.service.DebtorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/debtor")
public class DebtorController {

    @Autowired
    private DebtorService debtorService;

    @ResponseBody
    @RequestMapping(value = "")
    public List<Debtor> index() {
        return debtorService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/{id}")
    public Debtor show(@PathVariable Long id) {
        return debtorService.findOne(id);
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Debtor create(@RequestBody Debtor debtor) {
        return debtorService.save(debtor);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Long delete(@PathVariable Long id) {
        debtorService.delete(id);
        return id;
    }

}
