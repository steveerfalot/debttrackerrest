package com.mikeparksdev.debttracker.debt.controller;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.service.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/debt")
public class DebtController {

    @Autowired
    private DebtService debtService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<Debt> index() {
        return debtService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Debt show(@PathVariable Long id) {
        return debtService.findOne(id);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Debt create(@RequestBody Debt debt) {
        return debtService.save(debt);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id) {
        debtService.delete(id);
        return id.toString();
    }

}
