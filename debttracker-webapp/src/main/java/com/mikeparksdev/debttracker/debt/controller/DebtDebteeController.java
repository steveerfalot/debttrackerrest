package com.mikeparksdev.debttracker.debt.controller;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.service.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/debtee/{debteeId}/debt")
public class DebtDebteeController {

    @Autowired
    private DebtService debtService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Debt> index(@PathVariable Long debteeId){
        return debtService.findByDebteeId(debteeId);
    }

}
