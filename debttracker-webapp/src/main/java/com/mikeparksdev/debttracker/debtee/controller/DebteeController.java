package com.mikeparksdev.debttracker.debtee.controller;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtee.service.DebteeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/debtee")
public class DebteeController {

    @Autowired
    private DebteeService debteeService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<Debtee> index(){
        return debteeService.findAll();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Debtee create(@RequestBody Debtee debtee){
        return debteeService.save(debtee);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Debtee show(@PathVariable Long id){
        return debteeService.findById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id){
        debteeService.delete(id);
        return id.toString();
    }

}
