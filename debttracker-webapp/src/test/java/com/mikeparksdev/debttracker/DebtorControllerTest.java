package com.mikeparksdev.debttracker;

import com.mikeparksdev.debttracker.debtor.domain.Debtor;
import com.mikeparksdev.debttracker.debtor.service.DebtorService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebtorControllerTest {

    @Mock
    private DebtorService mockDebtorService;

    @InjectMocks
    private DebtorController subject;

    private List<Debtor> debtors;
    private Debtor debtor;

    private static final Long id = 1L;

    @Before
    public void setUp() {

        subject = new DebtorController();

        debtors = new ArrayList<Debtor>();
        debtor = new Debtor();
        debtor.setId(id);
        debtor.setName("Bob");

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testIndex_callsFindAll() {
        subject.index();
        verify(mockDebtorService).findAll();
    }

    @Test
    public void testIndex_returnsDebtors() {
        when(mockDebtorService.findAll()).thenReturn(debtors);
        List<Debtor> actual = subject.index();
        assertEquals(debtors, actual);
    }

    @Test
    public void testShow_callsFindOne() {
        subject.show(id);
        verify(mockDebtorService).findOne(id);
    }

    @Test
    public void testShow_returnsDebtor() {
        when(mockDebtorService.findOne(id)).thenReturn(debtor);
        Debtor actual = subject.show(id);
        assertEquals(debtor, actual);
    }

    @Test
    public void testSave_callsSave() {
        subject.create(debtor);
        verify(mockDebtorService).save(debtor);
    }

    @Test
    public void testSave_returnsDebtor() {
        when(mockDebtorService.save(debtor)).thenReturn(debtor);
        Debtor actual = subject.create(debtor);
        assertEquals(debtor, actual);
    }

    @Test
    public void testDelete_callsDelete() {
        subject.delete(debtor.getId());
        verify(mockDebtorService).delete(id);
    }

    @Test
    public void testDelete_callsDebtorId() {
        Long actual = subject.delete(debtor.getId());
        assertEquals(id, actual);
    }

}