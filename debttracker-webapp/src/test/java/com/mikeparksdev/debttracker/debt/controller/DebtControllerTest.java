package com.mikeparksdev.debttracker.debt.controller;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.service.DebtService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebtControllerTest {

    @Mock
    private DebtService mockDebtService;

    @InjectMocks
    private DebtController subject;

    private Debt debt;
    private List<Debt> debts;

    @Before
    public void setUp() throws Exception {
        subject = new DebtController();

        debt = new Debt();
        debts = new ArrayList<Debt>();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIndex_callsFindAll() {
        when(mockDebtService.findAll()).thenReturn(debts);

        subject.index();

        verify(mockDebtService).findAll();
    }

    @Test
    public void testIndex_returnsDebtList() {
        when(mockDebtService.findAll()).thenReturn(debts);

        List<Debt> actual = subject.index();

        assertEquals(debts, actual);
    }

    @Test
    public void testShow_callsFindOne() {
        when(mockDebtService.findOne(1L)).thenReturn(debt);

        subject.show(1L);

        verify(mockDebtService).findOne(1L);
    }

    @Test
    public void testShow_returnsDebt() {
        when(mockDebtService.findOne(1L)).thenReturn(debt);

        Debt actual = subject.show(1L);

        assertEquals(debt, actual);
    }

    @Test
    public void testCreate_callsSave() {
        when(mockDebtService.save(debt)).thenReturn(debt);

        subject.index();

        verify(mockDebtService).findAll();
    }

    @Test
    public void testCreate_returnsSavedDebt() {
        when(mockDebtService.save(debt)).thenReturn(debt);

        Debt actual = subject.create(debt);

        assertEquals(debt, actual);
    }

    @Test
    public void testDelete_callsDelete() {
        subject.delete(1L);

        verify(mockDebtService).delete(1L);
    }

    @Test
    public void testDelete_returnsIdOfDeletedDebt() {
        Long id = 1L;

        String actual = subject.delete(id);

        assertEquals(id.toString(), actual);
    }

}