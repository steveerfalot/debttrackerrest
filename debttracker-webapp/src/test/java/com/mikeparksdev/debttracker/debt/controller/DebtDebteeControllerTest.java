package com.mikeparksdev.debttracker.debt.controller;

import com.mikeparksdev.debttracker.debt.domain.Debt;
import com.mikeparksdev.debttracker.debt.service.DebtService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebtDebteeControllerTest {

    @Mock
    private DebtService mockDebtService;

    @InjectMocks
    private DebtDebteeController subject;

    private Debt debt;
    private List<Debt> debts;

    @Before
    public void setUp() throws Exception {
        subject = new DebtDebteeController();

        debt = new Debt();
        debt.setId(1L);
        debts = new ArrayList<Debt>();
        debts.add(debt);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIndex_callsFindByDebteeId() throws Exception {
        when(mockDebtService.findByDebteeId(2L)).thenReturn(debts);

        subject.index(2L);

        verify(mockDebtService).findByDebteeId(2L);
    }

    @Test
    public void testIndex_returnsListOfDebts() throws Exception {
        when(mockDebtService.findByDebteeId(2L)).thenReturn(debts);

        List<Debt> actual = subject.index(2L);

        assertEquals(debts, actual);
    }

}