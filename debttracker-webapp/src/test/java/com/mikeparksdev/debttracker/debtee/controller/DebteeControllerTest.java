package com.mikeparksdev.debttracker.debtee.controller;

import com.mikeparksdev.debttracker.debtee.domain.Debtee;
import com.mikeparksdev.debttracker.debtee.service.DebteeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DebteeControllerTest {

    @Mock
    private DebteeService mockDebteeService;

    @InjectMocks
    private DebteeController subject;

    private List<Debtee> debtees;
    private Debtee debtee;

    @Before
    public void setUp() throws Exception {
        debtee = new Debtee();
        debtee.setId(1L);
        debtee.setName("Dude Duderson");

        debtees = new ArrayList<Debtee>();
        debtees.add(debtee);

        subject = new DebteeController();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIndex_callsFindAll(){
        //arrange
        when(mockDebteeService.findAll()).thenReturn(new ArrayList<Debtee>());
        //act
        subject.index();
        //assert
        verify(mockDebteeService).findAll();
    }

    @Test
    public void testIndex_returnsDebteeList(){
        //arrange
        when(mockDebteeService.findAll()).thenReturn(debtees);
        //act
        List<Debtee> expected = subject.index();
        //assert
        assertEquals(expected, debtees);
    }

    @Test
    public void testCreate_callsCreate(){
        //arrange
        when(mockDebteeService.save(debtee)).thenReturn(debtee);
        //act
        subject.create(debtee);
        //assert
        verify(mockDebteeService).save(debtee);
    }

    @Test
    public void testCreate_returnsSavedDebtee(){
        //arrange
        when(mockDebteeService.save(debtee)).thenReturn(debtee);
        //act
        Debtee actual = subject.create(debtee);
        //assert
        assertEquals(debtee, actual);
    }

    @Test
    public void testShow_callsFindById(){
        //arrange
        when(mockDebteeService.findById(1L)).thenReturn(debtee);
        //act
        subject.show(1L);
        //assert
        verify(mockDebteeService).findById(1L);
    }

    @Test
    public void testShow_returnsDebtee(){
        //arrange
        when(mockDebteeService.findById(1L)).thenReturn(debtee);
        //act
        Debtee actual = subject.show(1L);
        //assert
        assertEquals(debtee, actual);
    }

    @Test
    public void testDelete_callsDelete(){
        subject.delete(1L);

        verify(mockDebteeService).delete(1L);
    }

    @Test
    public void testDelete_returnsIdOnSuccess(){
        String actual = subject.delete(1L);

        assertEquals(new Long(1).toString(), actual);
    }

}